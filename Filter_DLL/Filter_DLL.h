// Filter_DLL.h - Contains declarations of filter function
#pragma once

#ifdef FILTER_DLL_EXPORTS
#define FILTER_DLL_API __declspec(dllexport)
#else
#define FILTER_DLL_API __declspec(dllimport)
#endif

extern "C" FILTER_DLL_API unsigned char* filter_c(unsigned char* image, const size_t w, const size_t h, unsigned char* mask, const size_t mask_size);