#include"pch.h"
#include"Filter_DLL.h"

unsigned char* filter_c(unsigned char* image, const size_t w, const size_t h, unsigned char* mask, const size_t mask_size) {
	
	int mask_weights = 0;
	for (size_t i = 0; i < mask_size * mask_size; i++)
		mask_weights += mask[i];
	if (mask_weights == 0)
		mask_weights = 1;

	size_t my, mx;
	size_t x, y, p;

	unsigned char* pix = new unsigned char[w * h * 3];
	int r, g, b;
	size_t mask_p;
	int mask_i, mask_val = 1;
	for (y = 1; y < h - 1; y++) {
		for (x = 1; x < w - 1; x++) {
			p = (y * w + x) * 3;
			mask_i = 0;
			mask_p = p - (w + 1) * 3; // wiersz wy�ej i o jedno RGB wcze�niej (wzgl�dem pocz�tku).
			r = 0, g = 0, b = 0;
			for (my = 0; my < mask_size; my++) {
				for (mx = 0; mx < mask_size; mx++) {
					mask_val = mask[mask_i];

					r += image[mask_p] * mask_val;
					g += image[mask_p + 1] * mask_val;
					b += image[mask_p + 2] * mask_val;
					mask_p += 3; //przes�wam o RGB dalej
					mask_i++;
				}
				mask_p += (w - 3) * 3; //przes�wam do kolejnego wiersza i 2 RGB wcze�niej (wzgl�dem pivota).
			}
				
			pix[p] = r / mask_weights;
			pix[p + 1] = g / mask_weights;
			pix[p + 2] = b / mask_weights;
		}
	}

	//for (size_t i = 0; i < w*3; i+=3) {
	//	pix[i] = image[i];
	//	pix[i+1] = image[i+1];
	//	pix[i+2] = image[i+2];
	//}
		

	return pix;
}